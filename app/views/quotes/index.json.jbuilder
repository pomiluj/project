json.array!(@quotes) do |quote|
  json.extract! quote, :id, :amount, :interest, :principle, :total, :status
  json.url quote_url(quote, format: :json)
end
