class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.decimal :amount
      t.decimal :interest
      t.decimal :principle
      t.decimal :total
      t.string :status

      t.timestamps null: false
    end
  end
end
