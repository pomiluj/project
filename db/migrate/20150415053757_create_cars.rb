class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :model
      t.string :color
      t.string :VIN

      t.timestamps null: false
    end
  end
end
